package CommandFactory;

import Settings.Settings;

public class CommandFFMPEG {
	private static String Command;

	public static String getCommand(String inputFilePath, String outputFilePath) {
		// resolution// -vf scale=320:240
		// bit rate -b:v 64k
		String Command1 = Settings.WIN32_FFMPEG + " " + "-i " + inputFilePath;
		String resolution = " -vf scale=" + Settings.RESOLUTION_WIDTH + ":" + Settings.RESOLUTION_HEIGHT;
		String bitRate = " -b:v " + Settings.BITRATE * 1000 + "k";
		// Aspect Ratio -aspect 854:480
		String aspectRatio = " -aspect " + Settings.ASPECT_WIDTH + ":" + Settings.ASPECT_HEIGHT;
		String outputFile = " " + outputFilePath;

		Command = Command1 + resolution + bitRate + aspectRatio + outputFile;
		return Command;

	}

	/*
	 * public static void main(String p[]) {
	 * 
	 * System.out.println(getCommand("F:\\song.3gp", "F:\\converted.mp4")); }
	 */
}
