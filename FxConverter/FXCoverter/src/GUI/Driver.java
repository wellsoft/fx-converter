package GUI;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import Settings.Settings;
import helpers.fileClass;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class Driver extends Application implements Initializable {
	@FXML
	private Button myButton;

	@FXML
	private Button myButton2;
	Pane page;
	Stage primaryStage;
	@FXML
	TableView inputTable;
	ObservableList<fileClass> personData = FXCollections.observableArrayList();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		assert myButton != null : "fx:id=\"myButton\" was not injected: check your FXML file 'simple.fxml'.";
		assert inputTable != null : "fx:id=\"inputTable\" was not injected: check your FXML file 'simple.fxml'.";

		myButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				inputTable = new TableView();
				DirectoryChooser chooser = new DirectoryChooser();
				chooser.setTitle("Choose a Folder for Videos");
				File defaultDirectory = new File("F:\\songs");
				chooser.setInitialDirectory(defaultDirectory);
				File selectedDirectory = chooser.showDialog(primaryStage);
				TableColumn fileNameCol = new TableColumn("File Name");
				TableColumn fileExtensionCol = new TableColumn("File Type");
				TableColumn fileStatuscol = new TableColumn("File Status");
				inputTable.setEditable(true);
				inputTable.getColumns().addAll(fileNameCol, fileExtensionCol, fileStatuscol);
				if (selectedDirectory.getAbsolutePath() != null) {
					Settings.INPUT_DIR = selectedDirectory.getAbsolutePath();
					for (File file : selectedDirectory.listFiles()) {
						// personData.add(new fileClass(file.getName(),
						// file.getPath(), "Ready"));
						// inputTable.getItems().addAll(file.getName());
					}
				}
				// inputTable.getItems().addAll(personData);

			}
		});
		assert myButton2 != null : "fx:id=\"myButton2\" was not injected: check your FXML file 'simple.fxml'.";
		myButton2.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				DirectoryChooser chooser = new DirectoryChooser();
				chooser.setTitle("Choose a Output Folder for Videos");
				File defaultDirectory = new File("c:/");
				chooser.setInitialDirectory(defaultDirectory);
				File selectedDirectory = chooser.showDialog(primaryStage);
				if (selectedDirectory.getAbsolutePath() != null)
					Settings.OUTPUT_DIR = selectedDirectory.getAbsolutePath();
			}
		});
	}

	@Override
	public void start(Stage primaryStage) {

		try {
			// System.out.println("path is" +
			// Driver.class.getResource("/main.fxml").toString());
			page = (Pane) FXMLLoader.load(getClass().getClassLoader().getResource("main.fxml"));
			Scene scene = new Scene(page);
			this.primaryStage = primaryStage;
			primaryStage.setScene(scene);
			primaryStage.setTitle("FXML is Simple");
			primaryStage.show();

		} catch (Exception ex) {
			Logger.getLogger(Driver.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	public static void main(String[] args) {
		launch(args);
	}

}
